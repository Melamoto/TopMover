runProcess(function ()
  while true do
    spawnBullet(0, 1, 1, 2, true)
    spawnBullet(0, 2, 1, 2, true)
    spawnBullet(2, width, 1, 2, true)
    spawnBullet(2, width - 1, 1, 2, true)
    spawnBullet(1, 1, 1, 2, true)
    spawnBullet(3, height, 1, 2, true)
    waitSeconds(0.25)
  end
end)
waitSeconds(1)

runProcess(function ()
  while true do
    spawnBullet(1, 3, 2, 1)
    waitSeconds(4)
  end
end)

runProcess(function ()
  while true do
    local x = math.random(3, width - 2)
    local dir = ((math.random(2) - 1) * 2)
    spawnBullet(dir, x, 1, 1)
    waitSeconds(1)
  end
end)
