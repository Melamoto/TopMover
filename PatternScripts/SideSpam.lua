runProcess(function ()
  local dir = 3
  while true do
    local y = playerY
    for i=1,4,1 do
      spawnBullet(dir, y, 1, 2)
      waitSeconds(0.25)
    end
    dir = 4 - dir
  end
end)

runProcess(function ()
  local size = 2
  while true do
    local p = math.random(width)
    spawnBullet(0, p, 2)
    size = 3 - size
    waitSeconds(2)
  end
end)
