runProcess(function ()
  while true do
    local dir = (math.floor((playerX / width) + 0.5) * 2) + 1
    spawnBurst(dir, playerY, 2, 1)
    waitSeconds(2)
  end
end)

waitSeconds(1)

runProcess(function ()
  while true do
    local dir = 2 - (math.floor((playerY / height) + 0.5) * 2)
    spawnBurst(dir, playerX, 2, 1)
    waitSeconds(2)
  end
end)
