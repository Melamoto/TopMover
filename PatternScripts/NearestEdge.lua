runProcess(function ()
  while true do
    local dir = (math.floor((playerX / width) + 0.5) * 2) + 1
    spawnBullet(dir, playerY, 1, 2)
    waitSeconds(1)
  end
end)

runProcess(function ()
  while true do
    local dir = 2 - (math.floor((playerY / height) + 0.5) * 2)
    spawnBullet(dir, playerX, 1, 2)
    waitSeconds(1)
  end
end)
