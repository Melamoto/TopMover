// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "Virus.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FVirusEvent);

UCLASS()
class TOPMOVER_API AVirus : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	AVirus();

    UPROPERTY(BlueprintReadWrite, VisibleAnywhere)
    class UGridMovementComponent* GridMovement;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

    UFUNCTION(BlueprintImplementableEvent)
    void FinishMovementFX();
    UFUNCTION(BlueprintImplementableEvent)
    void StartMovementFX();
    UFUNCTION(BlueprintImplementableEvent)
    void MidMovementFX();

    void DetonateBomb();

    UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Virus | Bomb")
    int32 BombCount;

    UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Virus | Bomb")
    TSubclassOf<AActor> BombClass;

    UPROPERTY(BlueprintAssignable)
    FVirusEvent OnBombUsed;

    UPROPERTY(BlueprintCallable, BlueprintAssignable)
    FVirusEvent OnKilled;

    UFUNCTION(BlueprintCallable)
    void MoveInDirection(FVector Direction);

    UFUNCTION(BlueprintPure, BlueprintCallable)
    bool IsMoving();

    UFUNCTION(BlueprintPure, BlueprintCallable)
    FVector CurrentMovementDirection();

    UFUNCTION(BlueprintNativeEvent)
    bool CanMoveInDirection(FVector Direction);

    void OnMove();
    void TransitionUnsafe();
    void TransitionSafe();

    FVector GetBufferedMove();

    UPROPERTY(BlueprintReadOnly, EditAnywhere, Category = "Virus | Gameplay")
    bool bIsPrimary;

    UPROPERTY(BlueprintReadOnly, EditAnywhere, Category =  "Virus | Gameplay")
    bool bAffectedByPrimary;

    UFUNCTION(BlueprintCallable, Category = "Virus | Gameplay")
    void SetAffectedByPrimary(bool bIsAffected);

    UPROPERTY(BlueprintReadOnly, EditAnywhere, Category = "Virus | Gameplay")
    bool bAffectedBySecondary;

    UFUNCTION(BlueprintCallable, Category = "Virus | Gameplay")
    void SetAffectedBySecondary(bool bIsAffected);

    UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Virus | Gameplay")
    bool bInvincible;

    UFUNCTION(BlueprintCallable)
    void OnHitFirewall(bool bPrimary, bool bSecondary);

    UFUNCTION(BlueprintCallable)
    void KillVirus();
    UFUNCTION(BlueprintImplementableEvent)
    void KillVirusFX();

    UFUNCTION(BlueprintImplementableEvent)
    void RotateFX(float RotationPosition);

    UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Virus | Movement")
    float GetMovementStepTime();

    UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Virus | Movement")
    float MovementSafetyBuffer;
    UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Virus | Movement")
    float MoveStepSize;
    UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Virus | Movement")
    float BufferedMoveSpan;
    UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Virus | Movement")
    UCurveFloat* MovementCurve;

    UFUNCTION(BlueprintImplementableEvent)
    void DoFlip(FVector Axis);

    bool bIsMoving;

    UPROPERTY(Transient)
    FVector BufferedMove;
    UPROPERTY(Transient)
    bool BufferedFlip;
    UPROPERTY(Transient)
    float LastBufferTime;

    UPROPERTY(Transient)
    float TravelPosition;
    UPROPERTY(Transient)
    FVector PreviousPosition;
    UPROPERTY(Transient)
    FVector Destination;
    UPROPERTY(Transient)
    FQuat PreviousQuat;
    UPROPERTY(Transient)
    FQuat DestinationQuat;

    UFUNCTION(BlueprintCallable, BlueprintPure)
    class ATDPlayerState* GetTDPlayerState();
};
