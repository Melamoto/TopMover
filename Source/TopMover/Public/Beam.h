// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Beam.generated.h"

UCLASS()
class TOPMOVER_API ABeam : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ABeam();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

    UPROPERTY(BlueprintReadWrite, EditAnywhere, Meta = (Category = "Firewall", ExposeOnSpawn))
    float BeamLength;
    UPROPERTY(BlueprintReadWrite, EditAnywhere, Meta = (Category = "Firewall", ExposeOnSpawn))
    uint32 bIsPrimary : 1;
    UPROPERTY(BlueprintReadWrite, EditAnywhere, Meta = (Category = "Firewall", ExposeOnSpawn))
    uint32 bIsSecondary : 1;
};
