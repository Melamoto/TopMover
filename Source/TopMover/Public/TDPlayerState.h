// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerState.h"
#include "TDPlayerState.generated.h"

/**
 * 
 */
UCLASS()
class TOPMOVER_API ATDPlayerState : public APlayerState
{
	GENERATED_BODY()
public:
    ATDPlayerState();
    UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Gameplay | Status")
    int32 Lives;
    UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Gameplay | Status")
    int32 Bombs;
};
