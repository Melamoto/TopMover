// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/StaticMeshActor.h"
#include "GridCube.generated.h"

/**
 * 
 */
UCLASS()
class TOPMOVER_API AGridCube : public AStaticMeshActor
{
    GENERATED_BODY()
public:
    UFUNCTION(BlueprintImplementableEvent, BlueprintCallable)
    void Roll(FVector Direction);
	
	
};
