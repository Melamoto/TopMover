// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "PatternData.h"
#include "FileLib.generated.h"

/**
 * 
 */
UCLASS()
class TOPMOVER_API UFileLib : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()
public:
    UFUNCTION(BlueprintCallable, Category = "Lua")
    static bool LoadLuaScripts(FString& BaseScript, TArray<FPatternData>& PatternScripts, FString FolderPath);
	
};
