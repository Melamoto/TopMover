// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

extern "C" {
#include "lua.h"
#include "lauxlib.h"
#include "lualib.h"
}

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "PatternComponent.generated.h"

DECLARE_DELEGATE_FiveParams(FFirewallSpawnDelegate, int32, int32, float, float, bool);
DECLARE_DELEGATE_FiveParams(FBurstSpawnDelegate, int32, int32, int32, float, bool);
DECLARE_DELEGATE_FourParams(FBeamSpawnDelegate, int32, int32, float, bool);

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class TOPMOVER_API UPatternComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UPatternComponent();
    ~UPatternComponent();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

    UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Lua")
    FString ScriptCode;

    UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Pattern")
    int32 Width;
    UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Pattern")
    int32 Height;
    UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Pattern")
    float GetTickRate();

    UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Lua")
    bool bIsPaused;

    void LoadBaseScript();

    UFUNCTION(BlueprintCallable, Category = "Lua")
    void RunRoutine(FString Routine, FString DebugName = "Anonymous");

    UFUNCTION(BlueprintCallable, Category = "Lua")
    void ResetPattern();

    UFUNCTION()
    void UpdatePlayerTarget(int32 PlayerX, int32 PlayerY);

    FFirewallSpawnDelegate FirewallSpawnDelegate;
    FBurstSpawnDelegate BurstSpawnDelegate;
    FBeamSpawnDelegate BeamSpawnDelegate;

private:
    void TickLua(float DeltaTime);

    int SpawnBullet(lua_State* L);
    int SpawnBurst(lua_State* L);
    int SpawnBeam(lua_State* L);
    int LuaLog(lua_State* L);

    lua_State* LuaState;
};
