// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameStateBase.h"
#include "TMGameState.generated.h"

/**
 * 
 */
UCLASS()
class TOPMOVER_API ATMGameState : public AGameStateBase
{
	GENERATED_BODY()
public:
    ATMGameState();

    UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Gameplay | Timing")
    float Tempo;
	
    UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Gameplay | Timing")
    float GetTempoInBps();
};
