// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/MovementComponent.h"
#include "GridMovementComponent.generated.h"

DECLARE_MULTICAST_DELEGATE(FEventDelegate);
DECLARE_DELEGATE_RetVal_OneParam(bool, FValidMoveDelegate, FVector);

struct FMovementEvent {
    float Time;
    FEventDelegate Delegate;
};

UCLASS()
class TOPMOVER_API UGridMovementComponent : public UMovementComponent
{
	GENERATED_BODY()
public:
    UGridMovementComponent();

    virtual void TickComponent(float DeltaTime, enum ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction);

    float GetMovementStepTime();
    FEventDelegate& AddMovementEvent(float Time);
    void MoveInDirection(FVector Direction);

    UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Grid Movement")
    float CellWidth;
    UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Grid Movement")
    UCurveFloat* MovementCurve;
    UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Grid Movement")
    TArray<FVector> QueuedMoves;
    UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Grid Movement")
    float MovementRotation;

    bool bIsMoving;

    UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
    bool IsValidMove(FVector Direction);

    FValidMoveDelegate IsValidMoveDelegate;

    float GetRemainingMovementTime();

    FEventDelegate MoveStartedDelegate;

private:

    TArray<FMovementEvent> MovementEvents;

    UPROPERTY(Transient)
    float TravelPercentage;
    UPROPERTY(Transient)
    FVector PreviousLocation;
    UPROPERTY(Transient)
    FVector DestinationLocation;
    UPROPERTY(Transient)
    FQuat PreviousQuat;
    UPROPERTY(Transient)
    FQuat DestinationQuat;
};
