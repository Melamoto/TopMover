// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "GameFramework/Actor.h"
#include "Settings.generated.h"

UCLASS(Config = Game)
class TOPMOVER_API ASettings : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASettings();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
    UFUNCTION(BlueprintCallable, Category = "Settings")
    void SaveSettings();

    UPROPERTY(Config, BlueprintReadWrite, EditAnywhere, Category = "Settings | Volume")
    float MasterVolume;

    UPROPERTY(Config, BlueprintReadWrite, EditAnywhere, Category = "Settings | Volume")
    TMap<FString, float> VolumeSettings;
};
