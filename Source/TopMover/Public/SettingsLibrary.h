// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "SettingsLibrary.generated.h"

/**
 * 
 */
UCLASS()
class TOPMOVER_API USettingsLibrary : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()

public:
    UFUNCTION(BlueprintCallable, Category = "Settings")
    static bool GetSupportedScreenResolutions(TArray<FString>& Resolutions);

    /**
    * Pass it a correct string of "<width>x<height>" with valid 32 bit integer values, or perish
    * @param Resolution - The resolution string of the form "<width>x<height>"
    */
    UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Settings")
    static void ResolutionStringToValues(FString Resolution, int32& Width, int32& Height);

    UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Settings")
    static void ResolutionValuesToString(int32 Width, int32 Height, FString& Resolution);

    /**
    * Converts a string to the WindowMode enum type, defaulting to Windowed if no valid value provided
    * @param WindowModeString - Either Fullscreen, Windowed, or WindowedFullscreen
    */
    UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Settings")
    static EWindowMode::Type WindowModeStringToValue(FString WindowModeString);

    /**
    * Converts a WindowMode enum to a user-friendly string
    */
    UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Settings")
    static FString WindowModeToString(EWindowMode::Type WindowMode);
};
