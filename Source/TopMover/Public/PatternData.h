// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataTable.h"
#include "PatternData.generated.h"

USTRUCT(BlueprintType)
struct TOPMOVER_API FPatternData : public FTableRowBase
{
    GENERATED_BODY()

    FPatternData() {};

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Pattern, meta = (MultiLine = true))
    FString Name;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Pattern, meta = (MultiLine = true))
    FString Script;
};
