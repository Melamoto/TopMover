// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

extern "C" {
#include "lua.h"
#include "lauxlib.h"
#include "lualib.h"
}

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "LuaScript.generated.h"

/**
 * 
 */
UCLASS()
class TOPMOVER_API ULuaScript : public UObject
{
    GENERATED_BODY()
public:
    ULuaScript();
    ~ULuaScript();

    UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Lua")
    FString ScriptCode;

    void LoadScript();

    UFUNCTION(BlueprintCallable, Category = "Lua")
    int32 getNextValue();

private:
    lua_State* LuaState;
};
