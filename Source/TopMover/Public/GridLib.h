// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "Engine.h"
#include "GridCube.h"
#include "GridLib.generated.h"

USTRUCT(BlueprintType)
struct FGrid {
    GENERATED_BODY()

public:

    UPROPERTY(BlueprintReadWrite, EditAnywhere)
    float CellWidth;

    UPROPERTY(BlueprintReadWrite, EditAnywhere)
    int32 HorizontalCells;
    UPROPERTY(BlueprintReadWrite, EditAnywhere)
    int32 VerticalCells;

    UPROPERTY(BlueprintReadWrite, EditAnywhere)
    FVector GridBottomLeft;
};

/**
 * 
 */
UCLASS()
class TOPMOVER_API UGridLib : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()

public:
    UFUNCTION(BlueprintCallable, Category = "Grid | Spawn")
    static void SpawnNewGridCellDelayed(AActor* WorldContext, TSubclassOf<AGridCube> GridClass, FVector Location, FVector RollDirection, float Delay,
        FTimerHandle& TimerHandle, AGridCube*& GridCube);
    UFUNCTION(BlueprintCallable, Category = "Grid | Spawn")
    static AGridCube* SpawnNewGridCell(AActor* WorldContext, TSubclassOf<AGridCube> GridClass, FVector Location, FVector RollDirection);
    UFUNCTION(Category = "Grid | Spawn")
    static AGridCube* DeferredSpawnCube(AActor* WorldContext, TSubclassOf<AGridCube> GridClass, FVector Location, FVector RollDirection);
    UFUNCTION(Category = "Grid | Spawn")
    static void FinishSpawnCube(AGridCube* Cube, FVector RollDirection);

    UFUNCTION(BlueprintPure, BlueprintCallable, Category = "Grid")
    static int32 ManhattanDistance(FIntPoint A, FIntPoint B) {
        return FMath::Abs(A.X - B.X) + FMath::Abs(A.Y - B.Y);
    }
 
    UFUNCTION(BlueprintPure, BlueprintCallable, Category = "Grid")
    static FIntPoint GetRandomDistantCell(FGrid Grid, FIntPoint Centre, int32 MinDistance) {
        TArray<FIntPoint> DistantCells;
        for (int32 Vertical = 0; Vertical < Grid.VerticalCells; ++Vertical) {
            for (int32 Horizontal = 0; Horizontal < Grid.HorizontalCells; ++Horizontal) {
                FIntPoint CheckedPoint(Vertical, Horizontal);
                if (ManhattanDistance(Centre, CheckedPoint) >= MinDistance) {
                    DistantCells.Push(CheckedPoint);
                }
            }
        }
        int32 RandIndex = FMath::RandRange(0, DistantCells.Num() - 1);
        return DistantCells[RandIndex];
    }

    UFUNCTION(BlueprintPure, BlueprintCallable, Category = "Grid")
    static void GetClosestCellToPosition(FGrid Grid, FVector Position, int32& Vertical, int32& Horizontal) {
        FVector ScaledRelativePosition = (Position - Grid.GridBottomLeft) / Grid.CellWidth;
        Vertical = FMath::RoundToInt(ScaledRelativePosition.X);
        Horizontal = FMath::RoundToInt(ScaledRelativePosition.Y);
    }

    UFUNCTION(BlueprintPure, BlueprintCallable, Category = "Grid")
    static FVector GetCellPosition(FGrid Grid, int32 Vertical, int32 Horizontal) {
        return Grid.GridBottomLeft + (FVector(Vertical, Horizontal, 0.f) * Grid.CellWidth);
    }

    UFUNCTION(BlueprintPure, BlueprintCallable, Category = "Grid")
    static FVector GetVerticalBorderPosition(FGrid Grid, int32 Horizontal, bool Top, float Margin = 0.5f) {
        FVector Position = Grid.GridBottomLeft + (FVector(-Margin, Horizontal, 0.f) * Grid.CellWidth);
        if (Top) {
            Position.X += ((Grid.VerticalCells - 1) + (Margin * 2)) * Grid.CellWidth;
        }
        return Position;
    }

    UFUNCTION(BlueprintPure, BlueprintCallable, Category = "Grid")
    static FVector GetHorizontalBorderPosition(FGrid Grid, int32 Vertical, bool Right, float Margin = 0.5f) {
        FVector Position = Grid.GridBottomLeft + (FVector(Vertical, -Margin, 0.f) * Grid.CellWidth);
        if (Right) {
            Position.Y += ((Grid.HorizontalCells - 1) + (Margin * 2)) * Grid.CellWidth;
        }
        return Position;
    }

    UFUNCTION(BlueprintPure, BlueprintCallable, Category = "Grid")
    static FVector GetGridCenter(FGrid Grid) {
        return Grid.GridBottomLeft + (FVector(Grid.VerticalCells - 1, Grid.HorizontalCells - 1, 0.f) * Grid.CellWidth / 2);
    }

    UFUNCTION(BlueprintPure, BlueprintCallable, Category = "Grid")
    static FVector GetEdgeCell(FGrid Grid, int32 CellIndex, int32 Direction) {
        if (Direction == 0) return Grid.GridBottomLeft + (FVector(Grid.VerticalCells - 1, CellIndex, 0.f) * Grid.CellWidth);
        if (Direction == 1) return Grid.GridBottomLeft + (FVector(Grid.VerticalCells - 1 - CellIndex, Grid.HorizontalCells - 1, 0.f) * Grid.CellWidth);
        if (Direction == 2) return Grid.GridBottomLeft + (FVector(0.f, Grid.HorizontalCells - 1 - CellIndex, 0.f) * Grid.CellWidth);
        return Grid.GridBottomLeft + (FVector(CellIndex, 0.f, 0.f) * Grid.CellWidth);
        // return GetGridCenter(Grid) + (((GetDirectionVector(Direction) * (Direction % 2 == 0 ? Grid.HorizontalCells : Grid.VerticalCells)) + (GetDirectionVector(Direction + 1) * CellIndex)) * Grid.CellWidth);
    }

    static FVector GetDirectionVector(int32 Direction) {
        Direction = Direction % 4;
        if (Direction < 0) Direction = 4 + Direction;
        switch (Direction) {
            case 0: return FVector(1.f, 0.f, 0.f);
            case 1: return FVector(0.f, 1.f, 0.f);
            case 2: return FVector(-1.f, 0.f, 0.f);
            case 3: return FVector(0.f, -1.f, 0.f);
        }
        checkNoEntry();
        return FVector();
    }
};
