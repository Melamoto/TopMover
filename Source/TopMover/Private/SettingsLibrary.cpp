// Fill out your copyright notice in the Description page of Project Settings.

#include "SettingsLibrary.h"


bool USettingsLibrary::GetSupportedScreenResolutions(TArray<FString>& Resolutions) {
    FScreenResolutionArray ResolutionsArray;

    if (RHIGetAvailableResolutions(ResolutionsArray, true)) {
        for (const FScreenResolutionRHI& Resolution : ResolutionsArray) {
            float AspectRatio = (float)Resolution.Width / Resolution.Height;
            if (AspectRatio < 1.7f) {
                continue;
            }
            FString ResolutionString;
            ResolutionValuesToString(Resolution.Width, Resolution.Height, ResolutionString);
            Resolutions.AddUnique(ResolutionString);
        }
        return true;
    }
    return false;
}

void USettingsLibrary::ResolutionStringToValues(FString Resolution, int32& Width, int32& Height) {
    TArray<FString> Resolutions;
    int32 Length = Resolution.ParseIntoArray(Resolutions, TEXT("x"), true);
    if (Length != 2) {
        Width = 0;
        Height = 0;
        return;
    }
    Width = FCString::Atoi(*Resolutions[0]);
    Height = FCString::Atoi(*Resolutions[1]);
}

void USettingsLibrary::ResolutionValuesToString(int32 Width, int32 Height, FString& Resolution) {
    FString WidthString = FString::FromInt(Width);
    FString HeightString = FString::FromInt(Height);
    Resolution = WidthString + "x" + HeightString;
}

EWindowMode::Type USettingsLibrary::WindowModeStringToValue(FString WindowModeString) {
    if (WindowModeString.Equals("Fullscreen")) {
        return EWindowMode::Fullscreen;
    }
    if (WindowModeString.Equals("Windowed Fullscreen")) {
        return EWindowMode::WindowedFullscreen;
    }
    return EWindowMode::Windowed;
}

FString USettingsLibrary::WindowModeToString(EWindowMode::Type WindowMode) {
    if (WindowMode == EWindowMode::Fullscreen) {
        return TEXT("Fullscreen");
    }
    if (WindowMode == EWindowMode::WindowedFullscreen) {
        return TEXT("Windowed Fullscreen");
    }
    return TEXT("Windowed");
}
