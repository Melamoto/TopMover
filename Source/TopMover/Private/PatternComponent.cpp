// Fill out your copyright notice in the Description page of Project Settings.

#include "PatternComponent.h"
#include "FirewallSource.h"
#include "Engine/World.h"
#include "Kismet/GameplayStatics.h"

extern "C" {
#include "lua.h"
#include "lauxlib.h"
#include "lualib.h"
}

// Basic code for ticking co-routines in Lua from Jonathan Fischer (www.jonathanfischer.net/lua-coroutines/)
char* BaseLuaCode = R"***(
-- This table is indexed by coroutine and simply contains the time at which the coroutine
-- should be woken up.
local WAITING_ON_TIME = {}

-- Keep track of how long the game has been running.
local CURRENT_TIME = 0

function waitSeconds(seconds)
    -- Grab a reference to the current running coroutine.
    local co = coroutine.running()

    -- If co is nil, that means we're on the main process, which isn't a coroutine and can't yield
    assert(co ~= nil, "The main thread cannot wait!")

    -- Store the coroutine and its wakeup time in the WAITING_ON_TIME table
    local wakeupTime = CURRENT_TIME + seconds
    WAITING_ON_TIME[co] = wakeupTime

    -- And suspend the process
    return coroutine.yield(co)
end

function wakeUpWaitingThreads(deltaTime)
    -- This function should be called once per game logic update with the amount of time
    -- that has passed since it was last called
    CURRENT_TIME = CURRENT_TIME + deltaTime

    -- First, grab a list of the threads that need to be woken up. They'll need to be removed
    -- from the WAITING_ON_TIME table which we don't want to try and do while we're iterating
    -- through that table, hence the list.
    local threadsToWake = {}
    for co, wakeupTime in pairs(WAITING_ON_TIME) do
        if wakeupTime < CURRENT_TIME then
            table.insert(threadsToWake, co)
        end
    end

    -- Now wake them all up.
    for _, co in ipairs(threadsToWake) do
        WAITING_ON_TIME[co] = nil -- Setting a field to nil removes it from the table
        coroutine.resume(co)
    end
end

function killWaitingThreads()
    WAITING_ON_TIME = {}
end

function runProcess(func)
    -- This function is just a quick wrapper to start a coroutine.
    local co = coroutine.create(func)
    WAITING_ON_TIME[co] = CURRENT_TIME
    return co
end
)***";

typedef int (UPatternComponent::*LuaMethod)(lua_State* L);

template<LuaMethod Fn>
int DispatchLuaMethod(lua_State* L) {
    UPatternComponent* Pattern = *static_cast<UPatternComponent**>(lua_getextraspace(L));
    return ((*Pattern).*Fn)(L);
}

// Sets default values for this component's properties
UPatternComponent::UPatternComponent()
{
    // Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
    // off to improve performance if you don't need them.
    PrimaryComponentTick.bCanEverTick = true;

    Width = 9;
    Height = 5;

    bIsPaused = false;

    UE_LOG(LogTemp, Display, TEXT("PatternComponent: Base Lua Script Loaded."));
}

// Called when the game starts
void UPatternComponent::BeginPlay()
{
    Super::BeginPlay();

    LoadBaseScript();

    AFirewallSource* OwnerSource = Cast<AFirewallSource>(GetOwner());
    Width = OwnerSource->Grid.HorizontalCells;
    Height = OwnerSource->Grid.VerticalCells;
}

UPatternComponent::~UPatternComponent() {
    // lua_close(LuaState);
}

void UPatternComponent::LoadBaseScript() {
    LuaState = luaL_newstate();
    luaL_openlibs(LuaState);

    if (luaL_dostring(LuaState, TCHAR_TO_ANSI(*ScriptCode))) {
        const char* ErrorString = lua_tostring(LuaState, -1);
        if (ErrorString == nullptr) ErrorString = "unknown error";
        UE_LOG(LogTemp, Error, TEXT("Lua Script error: %s"), ANSI_TO_TCHAR(ErrorString));
        return;
    }
    *static_cast<UPatternComponent**>(lua_getextraspace(LuaState)) = this;

    lua_register(LuaState, "spawnBullet", &DispatchLuaMethod<&UPatternComponent::SpawnBullet>);
    lua_register(LuaState, "spawnBurst", &DispatchLuaMethod<&UPatternComponent::SpawnBurst>);
    lua_register(LuaState, "spawnBeam", &DispatchLuaMethod<&UPatternComponent::SpawnBeam>);
    lua_register(LuaState, "luaLog", &DispatchLuaMethod<&UPatternComponent::LuaLog>);

    if (luaL_dostring(LuaState, TCHAR_TO_ANSI(*FString::Printf(TEXT("width = %d\nheight = %d"), Width, Height)))) {
        const char* ErrorString = lua_tostring(LuaState, -1);
        if (ErrorString == nullptr) ErrorString = "unknown error";
        UE_LOG(LogTemp, Error, TEXT("UPatternComponent Lua Error while initiating globals: %s"), ANSI_TO_TCHAR(ErrorString));
    }
}

void UPatternComponent::RunRoutine(FString Routine, FString DebugName) {
    if (!LuaState) {
        return;
    }
    FString WrappedRoutine = "runProcess(function ()\n";
    WrappedRoutine += Routine;
    WrappedRoutine += "\nend)";
    if (luaL_dostring(LuaState, TCHAR_TO_ANSI(*WrappedRoutine))) {
        const char* ErrorString = lua_tostring(LuaState, -1);
        if (ErrorString == nullptr) ErrorString = "unknown error";
        UE_LOG(LogTemp, Error, TEXT("UPatternComponent Lua Error in RunRoutine \"%s\": %s"), *DebugName, ANSI_TO_TCHAR(ErrorString));
    }
}

void UPatternComponent::ResetPattern() {
    lua_getglobal(LuaState, "killWaitingThreads");
    lua_call(LuaState, 0, 0);
}

// Called every frame
void UPatternComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
    if (!bIsPaused) {
        TickLua(DeltaTime);
    }
}

int UPatternComponent::SpawnBullet(lua_State* L) {
    int32 ArgCount = lua_gettop(L);
    if (ArgCount < 3) {
        UE_LOG(LogTemp, Error, TEXT("'spawnBullet' called in Lua with %d arguments when at least 3 were expected."), ArgCount);
        return 0;
    }
    int32 Direction = lua_tointeger(L, 1);
    int32 Position = lua_tointeger(L, 2);
    float Width = lua_tonumber(L, 3);
    float SpeedMultiplier = ArgCount >= 4 ? lua_tonumber(L, 4) : 1.f;
    bool bDoubleColor = ArgCount >= 5 ? lua_toboolean(L, 5) : false;
    if (FirewallSpawnDelegate.IsBound()) {
        FirewallSpawnDelegate.ExecuteIfBound(Direction, Position - 1, Width, SpeedMultiplier, bDoubleColor);
    }
    return 0;
}

int UPatternComponent::SpawnBurst(lua_State* L) {
    int32 ArgCount = lua_gettop(L);
    if (ArgCount < 3) {
        UE_LOG(LogTemp, Error, TEXT("'spawnBurst' called in Lua with %d arguments when at least 3 were expected."), ArgCount);
        return 0;
    }
    int32 Direction = lua_tointeger(L, 1);
    int32 Position = lua_tointeger(L, 2);
    int32 BurstDistance = lua_tonumber(L, 3);
    float SpeedMultiplier = ArgCount >= 4 ? lua_tonumber(L, 4) : 1.f;
    bool bDoubleColor = ArgCount >= 5 ? lua_toboolean(L, 5) : false;
    if (BurstSpawnDelegate.IsBound()) {
        BurstSpawnDelegate.ExecuteIfBound(Direction, Position - 1, BurstDistance, SpeedMultiplier, bDoubleColor);
    }
    return 0;
}

int UPatternComponent::SpawnBeam(lua_State* L) {
    int32 ArgCount = lua_gettop(L);
    if (ArgCount < 2) {
        UE_LOG(LogTemp, Error, TEXT("'spawnBurst' called in Lua with %d arguments when at least 2 were expected."), ArgCount);
        return 0;
    }
    int32 Direction = lua_tointeger(L, 1);
    int32 Position = lua_tointeger(L, 2);
    float SpeedMultiplier = ArgCount >= 4 ? lua_tonumber(L, 3) : 1.f;
    bool bDoubleColor = ArgCount >= 5 ? lua_toboolean(L, 4) : false;
    if (BeamSpawnDelegate.IsBound()) {
        BeamSpawnDelegate.ExecuteIfBound(Direction, Position - 1, SpeedMultiplier, bDoubleColor);
    }
    return 0;
}

int UPatternComponent::LuaLog(lua_State* L) {
    int32 ArgCount = lua_gettop(L);
    if (ArgCount < 1) {
        UE_LOG(LogTemp, Error, TEXT("'logLua' called in Lua with %d arguments when at least 1 was expected."), ArgCount);
        return 0;
    }
    const char* LogString = lua_tostring(L, 1);
    if (LogString == nullptr) {
        UE_LOG(LogTemp, Error, TEXT("'logLua' called with invalid 1st argument; expected string."));
        return 0;
    }
    UE_LOG(LogTemp, Display, TEXT("Lua (PatternComponent %s): %s"), *GetName(), ANSI_TO_TCHAR(LogString));
    return 0;
}

float UPatternComponent::GetTickRate() {
    AFirewallSource* OwnerSource = Cast<AFirewallSource>(GetOwner());
    return OwnerSource ? OwnerSource->GetFirewallRate() : 1.f;
}

void UPatternComponent::TickLua(float DeltaTime) {
    lua_getglobal(LuaState, "wakeUpWaitingThreads");
    lua_pushnumber(LuaState, DeltaTime * GetTickRate());
    if (lua_pcall(LuaState, 1, 0, 0)) {
        const char* ErrorString = lua_tostring(LuaState, -1);
        if (ErrorString == nullptr) ErrorString = "unknown error";
        UE_LOG(LogTemp, Error, TEXT("UPatternComponent Lua Error during tick: %s"), ANSI_TO_TCHAR(ErrorString));
    }
}

void UPatternComponent::UpdatePlayerTarget(int32 PlayerX, int32 PlayerY) {
    if (luaL_dostring(LuaState, TCHAR_TO_ANSI(*FString::Printf(TEXT("playerX = %d\nplayerY = %d"), PlayerX + 1, PlayerY + 1)))) {
        const char* ErrorString = lua_tostring(LuaState, -1);
        if (ErrorString == nullptr) ErrorString = "unknown error";
        UE_LOG(LogTemp, Error, TEXT("UPatternComponent Lua Error while setting player coordinates: %s"), ANSI_TO_TCHAR(ErrorString));
    }
}
