// Fill out your copyright notice in the Description page of Project Settings.

#include "TMGameState.h"

constexpr float MinutesPerSecond = 1.f / 60.f;

ATMGameState::ATMGameState() {
    Tempo = 100.f;
}

float ATMGameState::GetTempoInBps() {
    return Tempo * MinutesPerSecond;
}
