// Fill out your copyright notice in the Description page of Project Settings.

#include "GridMovementComponent.h"
#include "TMGameState.h"
#include "Engine/World.h"

UGridMovementComponent::UGridMovementComponent() {
    CellWidth = 400.f;
    MovementRotation = PI;

    TravelPercentage = 0.f;
}

void UGridMovementComponent::TickComponent(float DeltaTime, enum ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) {
    if (ShouldSkipUpdate(DeltaTime)) {
        return;
    }
    Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
    if (!UpdatedComponent) {
        return;
    }

    if (!bIsMoving) {
        return;
    }
    float OldPercentage = TravelPercentage;
    TravelPercentage += DeltaTime / GetMovementStepTime();

    for (auto MovementEvent : MovementEvents) {
        if (OldPercentage < MovementEvent.Time && TravelPercentage >= MovementEvent.Time) {
            MovementEvent.Delegate.Broadcast();
        }
    }

    while (TravelPercentage >= 1.f) {
        FHitResult HitResult;
        SafeMoveUpdatedComponent(DestinationLocation - UpdatedComponent->GetComponentLocation(), DestinationQuat, true, HitResult);
        TravelPercentage -= 1.f;

        while (QueuedMoves.Num() > 0 && !IsValidMove(QueuedMoves[0])) {
            QueuedMoves.RemoveAt(0);
        }
        if (QueuedMoves.Num() > 0) {
            MoveInDirection(QueuedMoves[0]);
            QueuedMoves.RemoveAt(0);
        }
        else {
            TravelPercentage = 0.f;
            bIsMoving = false;
            return;
        }
    }

    FVector NewLocation = MovementCurve != nullptr
        ? FMath::Lerp(PreviousLocation, DestinationLocation, MovementCurve->GetFloatValue(TravelPercentage))
        : FMath::Lerp(PreviousLocation, DestinationLocation, TravelPercentage);

    FVector CurrentDirection = (DestinationLocation - PreviousLocation).GetSafeNormal();
    float Rotation = MovementCurve != nullptr
        ? MovementCurve->GetFloatValue(TravelPercentage)
        : FMath::InterpEaseIn(0.f, 1.f, TravelPercentage, 2.f);
    FQuat NewQuat = FMath::Lerp(PreviousQuat, DestinationQuat, Rotation);

    FHitResult HitResult;
    SafeMoveUpdatedComponent(NewLocation - UpdatedComponent->GetComponentLocation(), NewQuat, true, HitResult);
}

float UGridMovementComponent::GetRemainingMovementTime() {
    return GetMovementStepTime() * (1.f - TravelPercentage);
}

void UGridMovementComponent::MoveInDirection(FVector Direction) {
    if (!UpdatedComponent || !IsValidMove(Direction)) {
        return;
    }
    PreviousLocation = UpdatedComponent->GetComponentLocation();
    PreviousQuat = UpdatedComponent->GetComponentQuat();
    DestinationLocation = PreviousLocation + (Direction * CellWidth);
    DestinationQuat = PreviousQuat * FQuat(Direction, MovementRotation);
    bIsMoving = true;

    MoveStartedDelegate.Broadcast();
}

float UGridMovementComponent::GetMovementStepTime() {
    UWorld* World = GetWorld();
    check(World);
    ATMGameState* TMGameState = Cast<ATMGameState>(World->GetGameState());
    return TMGameState
        ? 0.5 * (1 / TMGameState->GetTempoInBps())
        : 0.25;
}

FEventDelegate& UGridMovementComponent::AddMovementEvent(float Time) {
    int32 Index = MovementEvents.Emplace();
    MovementEvents[Index].Time = Time;
    return MovementEvents[Index].Delegate;
}

bool UGridMovementComponent::IsValidMove_Implementation(FVector Direction) {
    if (Direction.IsNearlyZero()) {
        return false;
    }
    if (IsValidMoveDelegate.IsBound()) {
        return IsValidMoveDelegate.Execute(Direction);
    }
    return true;
}
