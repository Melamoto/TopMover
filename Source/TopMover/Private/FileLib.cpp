// Fill out your copyright notice in the Description page of Project Settings.

#include "FileLib.h"
#include "Misc/FileHelper.h"
#include "Misc/Paths.h"


bool UFileLib::LoadLuaScripts(FString& BaseScript, TArray<FPatternData>& PatternScripts, FString FolderPath) {
    FString FullScriptsPath = FPaths::Combine(FPaths::GameDir(), FolderPath);
    IFileManager& FileManager = IFileManager::Get();
    TArray<FString> FileNames;
    FileManager.FindFiles(FileNames, *FPaths::Combine(FullScriptsPath, FString("*.lua")), true, false);

    bool bBaseScriptFound = false;
    for (auto Filename : FileNames) {
        if (Filename == "BaseScript.lua") {
            bBaseScriptFound = true;
            FFileHelper::LoadFileToString(BaseScript, *FPaths::Combine(FullScriptsPath, Filename));
        }
        else {
            FPatternData Pattern;
            Pattern.Name = FPaths::GetBaseFilename(Filename);
            FFileHelper::LoadFileToString(Pattern.Script, *FPaths::Combine(FullScriptsPath, Filename));
            PatternScripts.Add(Pattern);
        }
    }
    return bBaseScriptFound;
}
