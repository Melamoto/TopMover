// Fill out your copyright notice in the Description page of Project Settings.

#include "Settings.h"


// Sets default values
ASettings::ASettings() {
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
    
    MasterVolume = 1.f;
    VolumeSettings.Add(TEXT("Music"), 1.f);
    VolumeSettings.Add(TEXT("SFX"), 1.f);
}

// Called when the game starts or when spawned
void ASettings::BeginPlay() {
	Super::BeginPlay();
    LoadConfig();
}

void ASettings::SaveSettings() {
    SaveConfig();
}
