// Fill out your copyright notice in the Description page of Project Settings.

#include "LuaScript.h"

extern "C" {
#include "lua.h"
#include "lauxlib.h"
#include "lualib.h"
}

ULuaScript::ULuaScript() {
    ScriptCode = R"***(
        local a = 5
        function getNext()
            a = a + 1
            return a
        end
    )***";
    LoadScript();
    UE_LOG(LogTemp, Warning, TEXT("Lua Script Loaded."));
}

ULuaScript::~ULuaScript() {
    // lua_close(LuaState);
}

void ULuaScript::LoadScript() {
    LuaState = luaL_newstate();
    luaL_openlibs(LuaState);

    int result = luaL_dostring(LuaState, TCHAR_TO_ANSI(*ScriptCode));
    if (result != 0) {
        UE_LOG(LogTemp, Error, TEXT("Lua Script error: %s"), ANSI_TO_TCHAR(lua_tostring(LuaState, -1)));
    }
}

int32 ULuaScript::getNextValue() {
    lua_getglobal(LuaState, "getNext");
    lua_call(LuaState, 0, 1);
    int32 next = lua_tointeger(LuaState, -1);
    lua_pop(LuaState, 1);
    return next;
}
