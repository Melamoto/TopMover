// Fill out your copyright notice in the Description page of Project Settings.

#include "Virus.h"
#include "GameFramework/FloatingPawnMovement.h"
#include "FirewallBase.h"
#include "Engine/World.h"
#include "TDPlayerState.h"
#include "TMGameState.h"
#include "Kismet/KismetMathLibrary.h"
#include "GridMovementComponent.h"


// Sets default values
AVirus::AVirus()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

    MoveStepSize = 400.f;

    LastBufferTime = 0.f;
    TravelPosition = 0.f;

    bIsMoving = false;
    bIsPrimary = false;
    bAffectedByPrimary = true;

    GridMovement = CreateDefaultSubobject<UGridMovementComponent>(FName("Grid Movement"));
}

// Called when the game starts or when spawned
void AVirus::BeginPlay() {
	Super::BeginPlay();
    PreviousPosition = GetActorLocation();
    Destination = GetActorLocation();

    GridMovement->IsValidMoveDelegate.BindUObject(this, &AVirus::CanMoveInDirection);
    GridMovement->MoveStartedDelegate.AddUObject(this, &AVirus::OnMove);
    GridMovement->AddMovementEvent(MovementSafetyBuffer).AddUObject(this, &AVirus::TransitionUnsafe);
    GridMovement->AddMovementEvent(0.5f).AddUObject(this, &AVirus::MidMovementFX);
    GridMovement->AddMovementEvent(1.f - MovementSafetyBuffer).AddUObject(this, &AVirus::TransitionSafe);
}

void AVirus::OnMove() {
    bIsPrimary = !bIsPrimary;
    StartMovementFX();
}
void AVirus::TransitionUnsafe() {
    if (bIsPrimary) SetAffectedBySecondary(true);
    else SetAffectedByPrimary(true);
}
void AVirus::TransitionSafe() {
    if (bIsPrimary) SetAffectedByPrimary(false);
    else SetAffectedBySecondary(false);
}

// Called every frame
void AVirus::Tick(float DeltaTime) {
	Super::Tick(DeltaTime);
}

// Called to bind functionality to input
void AVirus::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent) {
	Super::SetupPlayerInputComponent(PlayerInputComponent);

    InputComponent->BindAction("Bomb", IE_Pressed, this, &AVirus::DetonateBomb);

    InputComponent->BindAction<TBaseDelegate<void, FVector>>("MoveUp", IE_Pressed, this, &AVirus::MoveInDirection, FVector(1.f, 0.f, 0.f));
    InputComponent->BindAction<TBaseDelegate<void, FVector>>("MoveDown", IE_Pressed, this, &AVirus::MoveInDirection, FVector(-1.f, 0.f, 0.f));
    InputComponent->BindAction<TBaseDelegate<void, FVector>>("MoveLeft", IE_Pressed, this, &AVirus::MoveInDirection, FVector(0.f, -1.f, 0.f));
    InputComponent->BindAction<TBaseDelegate<void, FVector>>("MoveRight", IE_Pressed, this, &AVirus::MoveInDirection, FVector(0.f, 1.f, 0.f));
}

void AVirus::SetAffectedByPrimary(bool bIsAffected) {
    bAffectedByPrimary = bIsAffected;
    if (bAffectedByPrimary) {
        TArray<AActor*> OverlappingFirewalls;
        GetOverlappingActors(OverlappingFirewalls, AFirewallBase::StaticClass());
        for (auto Firewall : OverlappingFirewalls) {
            AFirewallBase* FirewallBase = Cast<AFirewallBase>(Firewall);
            if (FirewallBase->bIsActive) {
                OnHitFirewall(FirewallBase->bIsPrimary, FirewallBase->bIsSecondary);
            }
        }
    }
}
void AVirus::SetAffectedBySecondary(bool bIsAffected) {
    bAffectedBySecondary = bIsAffected;
    if (bAffectedBySecondary) {
        TArray<AActor*> OverlappingFirewalls;
        GetOverlappingActors(OverlappingFirewalls, AFirewallBase::StaticClass());
        for (auto Firewall : OverlappingFirewalls) {
            AFirewallBase* FirewallBase = Cast<AFirewallBase>(Firewall);
            if (FirewallBase->bIsActive) {
                OnHitFirewall(FirewallBase->bIsPrimary, FirewallBase->bIsSecondary);
            }
        }
    }
}

void AVirus::DetonateBomb() {
    ATDPlayerState* PlayerState = GetTDPlayerState();
    if (!PlayerState || PlayerState->Bombs<= 0) {
        return;
    }
    PlayerState->Bombs -= 1;
    UWorld* World = GetWorld();
    check(World);
    World->SpawnActor<AActor>(BombClass, GetActorLocation(), FRotator::ZeroRotator);
    OnBombUsed.Broadcast();
}

void AVirus::MoveInDirection(FVector Direction) {
    if (GridMovement->bIsMoving) {
        if (GridMovement->GetRemainingMovementTime() <= BufferedMoveSpan) {
            if (GridMovement->QueuedMoves.Num() > 0) {
                GridMovement->QueuedMoves[0] = Direction;
            }
            else {
                GridMovement->QueuedMoves.Add(Direction);
            }
        }
        return;
    }
    GridMovement->MoveInDirection(Direction);
}

bool AVirus::IsMoving() {
    return GridMovement->bIsMoving;
}

float AVirus::GetMovementStepTime() {
    UWorld* World = GetWorld();
    check(World);
    ATMGameState* TMGameState = Cast<ATMGameState>(World->GetGameState());
    return TMGameState
        ? 0.5 * (1 / TMGameState->GetTempoInBps())
        : 0.25;
}

FVector AVirus::CurrentMovementDirection() {
    if (!IsMoving()) {
        return FVector::ZeroVector;
    }
    FVector DestinationDelta = Destination - GetActorLocation();
    DestinationDelta.Normalize();
    return DestinationDelta;
}

FVector AVirus::GetBufferedMove() {
    UWorld* World = GetWorld();
    check(World);
    float CurrentTime = World->TimeSeconds;
    return CurrentTime - LastBufferTime > BufferedMoveSpan
        ? FVector::ZeroVector
        : BufferedMove;
}

bool AVirus::CanMoveInDirection_Implementation(FVector Direction) {
    return true;
}

void AVirus::OnHitFirewall(bool bPrimary, bool bSecondary) {
    if (!bInvincible && ((bPrimary && bAffectedByPrimary) || (bSecondary && bAffectedBySecondary))) {
        KillVirus();
    }
}

void AVirus::KillVirus() {
    KillVirusFX();
    Destroy();
}

ATDPlayerState* AVirus::GetTDPlayerState() {
    APlayerState* PlayerState = GetPlayerState();
    return Cast<ATDPlayerState>(PlayerState);
}
