// Fill out your copyright notice in the Description page of Project Settings.

#include "GridLib.h"
#include "Kismet/GameplayStatics.h"

AGridCube* UGridLib::DeferredSpawnCube(AActor* WorldContext, TSubclassOf<AGridCube> GridClass, FVector Location, FVector RollDirection) {
    FActorSpawnParameters GridSpawnParameters;
    GridSpawnParameters.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
    FVector SpawnLocation = Location - (RollDirection * 400);
    UWorld* World = WorldContext->GetWorld();
    check(World);
    AGridCube* Cube = World->SpawnActorDeferred<AGridCube>(GridClass, FTransform(Location));
    Cube->SetActorHiddenInGame(true);
    return Cube;
}

void UGridLib::FinishSpawnCube(AGridCube* Cube, FVector RollDirection) {
    FTransform Transform(Cube->GetActorLocation());
    UGameplayStatics::FinishSpawningActor(Cube, Transform);
    Cube->SetActorHiddenInGame(false);
    Cube->Roll(RollDirection);
}

void UGridLib::SpawnNewGridCellDelayed(AActor* WorldContext, TSubclassOf<AGridCube> GridClass, FVector Location, FVector RollDirection, float Delay,
    FTimerHandle& TimerHandle, class AGridCube*& GridCube) {
    if (Delay == 0.f) {
        GridCube = SpawnNewGridCell(WorldContext, GridClass, Location, RollDirection);
        return;
    }
    FTimerDelegate SpawnDelegate;

    GridCube = DeferredSpawnCube(WorldContext, GridClass, Location, RollDirection);
    SpawnDelegate.BindStatic(UGridLib::FinishSpawnCube, GridCube, RollDirection);
    UWorld* World = WorldContext->GetWorld();
    check(World);
    World->GetTimerManager().SetTimer(TimerHandle, SpawnDelegate, Delay, false);
}

AGridCube* UGridLib::SpawnNewGridCell(AActor* WorldContext, TSubclassOf<AGridCube> GridClass, FVector Location, FVector RollDirection) {
    FActorSpawnParameters GridSpawnParameters;
    GridSpawnParameters.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
    FVector SpawnLocation = Location - (RollDirection * 400);
    UWorld* World = WorldContext->GetWorld();
    check(World);
    AGridCube* Cube = World->SpawnActor<AGridCube>(GridClass, SpawnLocation, FRotator::ZeroRotator, GridSpawnParameters);
    Cube->Roll(RollDirection);
    return Cube;
}
