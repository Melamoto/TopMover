// Fill out your copyright notice in the Description page of Project Settings.

#include "FirewallSource.h"
#include "FirewallBase.h"
#include "Beam.h"
#include "Kismet/GameplayStatics.h"
#include "PatternComponent.h"
#include "Engine/World.h"
#include "TMGameState.h"


// Sets default values
AFirewallSource::AFirewallSource()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
    
    InitialSpeedMultiplier = 1.f;
    InitialTravelTime = 3.f;

    GridBody = CreateDefaultSubobject<UBoxComponent>(FName("Grid Body"));
    RootComponent = GridBody;

    PatternComponent = CreateDefaultSubobject<UPatternComponent>(FName("Patterns"));
}

// Called when the game starts or when spawned
void AFirewallSource::BeginPlay()
{
    PatternComponent->Width = Grid.HorizontalCells;
    PatternComponent->Height = Grid.VerticalCells;

	Super::BeginPlay();
    BaseSpeedMultiplier = InitialSpeedMultiplier;
    BaseTravelTime = InitialTravelTime;

    PatternComponent->FirewallSpawnDelegate.BindUObject(this, &AFirewallSource::OnSpawnFirewall);
    PatternComponent->BurstSpawnDelegate.BindUObject(this, &AFirewallSource::OnSpawnBurst);
    PatternComponent->BeamSpawnDelegate.BindUObject(this, &AFirewallSource::OnSpawnBeam);

    FTimerHandle TimerHandle;
    UpdatePlayerTarget();
    UWorld* World = GetWorld();
    check(World);
    World->GetTimerManager().SetTimer(TimerHandle, this, &AFirewallSource::UpdatePlayerTarget, 0.1, true);
}

void AFirewallSource::ResetFirewalls() {
    UWorld* World = GetWorld();
    check(World);
    FTimerManager& WorldTimerManager = World->GetTimerManager();
    for (FTimerHandle TimerHandle : SpawnTimerHandles) {
        WorldTimerManager.ClearTimer(TimerHandle);
    }
    PatternComponent->ResetPattern();
}

// Called every frame
void AFirewallSource::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
    UWorld* World = GetWorld();
    check(World);
    FTimerManager& WorldTimerManager = World->GetTimerManager();
    SpawnTimerHandles.RemoveAll([&](FTimerHandle TimerHandle) {
        return !WorldTimerManager.IsTimerActive(TimerHandle);
    });
}

AFirewallBase* AFirewallSource::SpawnFirewall(FTransform SpawnTransform, float TravelLength, bool bIsPrimary, bool bIsSecondary, float SpeedMultiplier, FRotator RotationOffset) {
    UWorld* World = GetWorld();
    check(World);
    AFirewallBase* Firewall = World->SpawnActorDeferred<AFirewallBase>(FirewallClass, SpawnTransform);
    Firewall->TravelLength = TravelLength;
    Firewall->TravelSpeed = GetFirewallRate() * Grid.CellWidth * SpeedMultiplier;
    Firewall->bIsPrimary = bIsPrimary;
    Firewall->bIsSecondary = bIsSecondary;
    Firewall->RotationOffset = RotationOffset;
    UGameplayStatics::FinishSpawningActor(Firewall, SpawnTransform);
    return Firewall;
}

AFirewallBase* AFirewallSource::SpawnBurst(FTransform SpawnTransform, float BurstDistance, bool bIsPrimary, bool bIsSecondary, float SpeedMultiplier, FRotator RotationOffset) {
    UWorld* World = GetWorld();
    check(World);
    AFirewallBase* Firewall = World->SpawnActorDeferred<AFirewallBase>(BurstClass, SpawnTransform);
    Firewall->TravelLength = BurstDistance;
    Firewall->TravelSpeed = GetFirewallRate() * Grid.CellWidth * SpeedMultiplier;
    Firewall->bIsPrimary = bIsPrimary;
    Firewall->bIsSecondary = bIsSecondary;
    Firewall->RotationOffset = RotationOffset;
    UGameplayStatics::FinishSpawningActor(Firewall, SpawnTransform);
    return Firewall;
}

ABeam* AFirewallSource::SpawnBeam(FTransform SpawnTransform, float BeamLength, bool bIsPrimary, bool bIsSecondary, float SpeedMultiplier) {
    UWorld* World = GetWorld();
    check(World);
    ABeam* Beam = World->SpawnActorDeferred<ABeam>(BeamClass, SpawnTransform);
    Beam->BeamLength = BeamLength;
    Beam->bIsPrimary = bIsPrimary;
    Beam->bIsSecondary = bIsSecondary;
    UGameplayStatics::FinishSpawningActor(Beam, SpawnTransform);
    return Beam;
}

float AFirewallSource::GetFirewallRate() {
    UWorld* World = GetWorld();
    check(World);
    ATMGameState* TMGameState = Cast<ATMGameState>(World->GetGameState());
    return TMGameState
        ? TMGameState->GetTempoInBps()
        : 2.f;
}

void AFirewallSource::OnSpawnFirewall(int32 Direction, int32 Position, float Width, float SpeedMultiplier, bool bDoubleColor) {
    bool bIsHorizontal = Direction % 2;
    FVector StartingPosition = bIsHorizontal ?
        UGridLib::GetHorizontalBorderPosition(Grid, Position, Direction == 3) :
        UGridLib::GetVerticalBorderPosition(Grid, Position, Direction == 0);
    FVector Shape = FVector(0.5, (Width * 8) - 6, 1.f);
    float Rotation = (Direction > 1 ? 0.f : 180.f) + (bIsHorizontal ? -90.f : 0.f);
    FTransform SpawnTransform(FRotator(0.f, Rotation, 0.f), StartingPosition, Shape);

    float TravelLength = Grid.CellWidth * (bIsHorizontal ? Grid.HorizontalCells : Grid.VerticalCells);

    SpawnFirewall(SpawnTransform, TravelLength, bDoubleColor || !bIsHorizontal, bDoubleColor || bIsHorizontal, SpeedMultiplier, FRotator::ZeroRotator);
}

void AFirewallSource::OnSpawnBurst(int32 Direction, int32 Position, int32 BurstDistance, float SpeedMultiplier, bool bDoubleColor) {
    bool bIsHorizontal = Direction % 2;
    FVector StartingPosition = bIsHorizontal ?
        UGridLib::GetHorizontalBorderPosition(Grid, Position, Direction == 3, 1.f) :
        UGridLib::GetVerticalBorderPosition(Grid, Position, Direction == 0, 1.f);
    FVector Shape = FVector(0.5, 2.f, 1.f);
    float Rotation = (Direction > 1 ? 0.f : 180.f) + (bIsHorizontal ? -90.f : 0.f);
    FTransform SpawnTransform(FRotator(0.f, Rotation, 0.f), StartingPosition, Shape);

    SpawnBurst(SpawnTransform, Grid.CellWidth * BurstDistance, bDoubleColor || !bIsHorizontal, bDoubleColor || bIsHorizontal, SpeedMultiplier, FRotator::ZeroRotator);
}

void AFirewallSource::OnSpawnBeam(int32 Direction, int32 Position, float SpeedMultiplier, bool bDoubleColor) {
    bool bIsHorizontal = Direction % 2;
    FVector StartingPosition = bIsHorizontal ?
        UGridLib::GetHorizontalBorderPosition(Grid, Position, Direction == 3, 1.f) :
        UGridLib::GetVerticalBorderPosition(Grid, Position, Direction == 0, 1.f);
    FVector Shape = FVector(1.f, 1.f, 1.f);
    float Rotation = (Direction > 1 ? 0.f : 180.f) + (bIsHorizontal ? -90.f : 0.f);
    FTransform SpawnTransform(FRotator(0.f, Rotation, 0.f), StartingPosition, Shape);

    float TravelLength = Grid.CellWidth * ((bIsHorizontal ? Grid.HorizontalCells : Grid.VerticalCells) + 1);

    SpawnBeam(SpawnTransform, TravelLength, bDoubleColor || !bIsHorizontal, bDoubleColor || bIsHorizontal, SpeedMultiplier);
}

void AFirewallSource::UpdatePlayerTarget() {
    APawn* PlayerPawn = UGameplayStatics::GetPlayerPawn(this, 0);
    if (!PlayerPawn) return;
    FVector RelativeScaledPosition = (PlayerPawn->GetActorLocation() - Grid.GridBottomLeft) / Grid.CellWidth;
    // Not an incorrect assignment, X and Y in World Space simply represent the vertical and horizontal grid dimensions respectively
    int32 PlayerY = FMath::FloorToInt(RelativeScaledPosition.X);
    int32 PlayerX = FMath::FloorToInt(RelativeScaledPosition.Y);
    PatternComponent->UpdatePlayerTarget(PlayerX, PlayerY);
}
