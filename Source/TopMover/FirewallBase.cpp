// Fill out your copyright notice in the Description page of Project Settings.

#include "FirewallBase.h"


// Sets default values
AFirewallBase::AFirewallBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

    bIsPrimary = true;
    bIsSecondary = false;
    bIsActive = true;

    RotationRate = 5.f;
}

// Called when the game starts or when spawned
void AFirewallBase::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AFirewallBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
    if (!GetVelocity().IsNearlyZero()) {
        RotateBodyTowardsVelocity(DeltaTime);
    }
}

void AFirewallBase::RotateBodyTowardsVelocity(float DeltaSeconds) {
    USceneComponent* Body = GetBody();
    if (Body == nullptr) {
        return;
    }
    FQuat CurrentQuat = (Body->GetComponentRotation() + RotationOffset.GetInverse()).Quaternion();
    FQuat TargetQuat = FMatrix(GetVelocity().GetSafeNormal(), GetActorRightVector(),
        FVector::CrossProduct(GetVelocity().GetSafeNormal(), GetActorRightVector()), FVector::ZeroVector).ToQuat(); //GetVelocity().ToOrientationQuat();
    if (CurrentQuat.Equals(TargetQuat)) {
        return;
    }
    float AngularDistance = CurrentQuat.AngularDistance(TargetQuat);
    float AngularMovement = DeltaSeconds * RotationRate;
    FQuat NewQuat = AngularMovement >= AngularDistance ?
        TargetQuat :
        FQuat::Slerp(CurrentQuat, TargetQuat, AngularMovement / AngularDistance);
    Body->SetWorldRotation(NewQuat.Rotator() + RotationOffset);
}

USceneComponent* AFirewallBase::GetBody_Implementation() {
    return nullptr;
}
