// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "TopMoverGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class TOPMOVER_API ATopMoverGameModeBase : public AGameModeBase
{
    GENERATED_BODY()

public:
    ATopMoverGameModeBase();
	
    virtual void RestartPlayer(class AController* NewPlayer) override;
	
    virtual void BeginPlay() override;
    virtual void Tick(float DeltaSeconds) override;

    UFUNCTION()
    void OnPlayerKilled();

    UFUNCTION(BlueprintCallable, Category = "Gameplay")
    void ClearFirewalls(bool bPhaseFirewallsOut);

    UFUNCTION(BlueprintCallable, BlueprintImplementableEvent)
    void ResumeProgress();
    UFUNCTION(BlueprintCallable, BlueprintImplementableEvent)
    void StartProgress();
    UFUNCTION(BlueprintCallable, BlueprintImplementableEvent)
    void PauseProgress();

    // Game Status
    UFUNCTION(BlueprintCallable, Category = "Gameplay | Player")
    void LosePlayerLife();
    UFUNCTION(BlueprintImplementableEvent, Category = "Gameplay | Player")
    void OnLifeLost();

    UFUNCTION(BlueprintImplementableEvent)
    void RespawnPlayer();
    UFUNCTION(BlueprintImplementableEvent)
    void GameOver();

    UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Gameplay | Player")
    int32 RemainingLives;
    UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Gameplay | Player")
    int32 StartingLifeCount;
    UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Gameplay | Player")
    int32 StartingBombCount;

    UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Gameplay | Game State")
    float GameTime;
    
    bool bGameTimerActive;

    UFUNCTION(BlueprintCallable, Category = "Gameplay | Game State")
    void PauseGameTimer();
    UFUNCTION(BlueprintCallable, Category = "Gameplay | Game State")
    void ResumeGameTimer();
    UFUNCTION(BlueprintCallable, Category = "Gameplay | Game State")
    void RestartGameTimer();

    UFUNCTION(BlueprintCallable, BlueprintPure)
    class ATDPlayerState* GetPlayerState(int32 PlayerIndex);
};
