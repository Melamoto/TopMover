// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "GridLib.h"
#include "FirewallSource.generated.h"

USTRUCT(BlueprintType)
struct FPatternItem {
    GENERATED_BODY()

public:

    FPatternItem() : TimeSteps(0.f), RelativePosition(0), RotationOffset(0) {}

    // The time in the pattern at which this appears; this is not an exact time, but will be scaled by speed
    UPROPERTY(BlueprintReadWrite, EditAnywhere)
    float TimeSteps;

    // Shape of the Firewall; X = depth, Y = width
    UPROPERTY(BlueprintReadWrite, EditAnywhere)
    FVector Shape;

    // Rotation offset of the Firewall
    UPROPERTY(BlueprintReadWrite, EditAnywhere)
    FRotator RotationOffset;

    // Describes the position of this item in number of places to the right of the start position
    UPROPERTY(BlueprintReadWrite, EditAnywhere)
    int32 RelativePosition;
};

USTRUCT(BlueprintType)
struct FPattern {
    GENERATED_BODY()

public:

    UPROPERTY(BlueprintReadWrite, EditAnywhere)
    TArray<FPatternItem> Items;

    float GetDuration() {
        float Duration = 0.f;
        for (auto Item : Items) {
            float ItemDelay = Item.TimeSteps;
            if (ItemDelay > Duration) {
                Duration = ItemDelay;
            }
        }
        return Duration;
    }

    int32 GetWidth() {
        int32 Leftmost = 0;
        int32 Rightmost = 0;
        for (auto Item : Items) {
            int32 ItemPosition = Item.RelativePosition;
            int32 ItemWidth = FMath::FloorToInt(Item.Shape.Y) / 8;
            if (ItemPosition + ItemWidth > Rightmost) {
                Rightmost = ItemPosition + ItemWidth;
            }
            if (ItemPosition - ItemWidth < Leftmost) {
                Leftmost = ItemPosition - ItemWidth;
            }
        }
        return Rightmost - Leftmost;
    };

    int32 GetOffset() {
        int32 Leftmost = 0;
        for (auto Item : Items) {
            int32 ItemPosition = Item.RelativePosition;
            int32 ItemWidth = FMath::FloorToInt(Item.Shape.Y) / 8;
            if (ItemPosition - ItemWidth < Leftmost) {
                Leftmost = ItemPosition - ItemWidth;
            }
        }
        return -Leftmost;
    };
};

UCLASS()
class TOPMOVER_API AFirewallSource : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AFirewallSource();

    UPROPERTY(BlueprintReadOnly, VisibleAnywhere)
    class UBoxComponent* GridBody;

    UPROPERTY(BlueprintReadOnly, VisibleAnywhere)
    class UPatternComponent* PatternComponent;

    UPROPERTY(BlueprintReadWrite, EditAnywhere, Meta = (Category = "Firewalls | Spawn", ExposeOnSpawn))
    TSubclassOf<class AFirewallBase> FirewallClass;
    UPROPERTY(BlueprintReadWrite, EditAnywhere, Meta = (Category = "Firewalls | Spawn", ExposeOnSpawn))
    TSubclassOf<class AFirewallBase> BurstClass;
    UPROPERTY(BlueprintReadWrite, EditAnywhere, Meta = (Category = "Firewalls | Spawn", ExposeOnSpawn))
    TSubclassOf<class ABeam> BeamClass;

    UPROPERTY(BlueprintReadWrite, EditAnywhere, Meta = (Category = "Firewalls | Spawn", ExposeOnSpawn))
    TArray<FPattern> Patterns;

    UPROPERTY(BlueprintReadWrite, EditAnywhere, Meta = (Category = "Firewalls | Spawn", ExposeOnSpawn))
    FGrid Grid;

    UFUNCTION(BlueprintCallable, BlueprintPure, Meta = (Category = "Firewalls | Spawn"))
    float GetFirewallRate();

    UPROPERTY(BlueprintReadWrite, EditAnywhere, Meta = (Category = "Firewalls | Spawn"))
    float InitialSpeedMultiplier;

    UPROPERTY(BlueprintReadWrite, EditAnywhere, Meta = (Category = "Firewalls | Spawn"))
    float InitialTravelTime;

    UPROPERTY(BlueprintReadWrite, EditInstanceOnly, Meta = (Category = "Firewalls | Spawn"))
    float BaseSpeedMultiplier;

    UPROPERTY(BlueprintReadWrite, EditInstanceOnly, Meta = (Category = "Firewalls | Spawn"))
    float BaseTravelTime;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

    UFUNCTION(BlueprintCallable)
    void ResetFirewalls();

    class AFirewallBase* SpawnFirewall(FTransform SpawnTransform, float TravelLength, bool bIsPrimary, bool bIsSecondary, float SpeedMultiplier, FRotator RotationOffset);
    class AFirewallBase* SpawnBurst(FTransform SpawnTransform, float BurstDistance, bool bIsPrimary, bool bIsSecondary, float SpeedMultiplier, FRotator RotationOffset);
    class ABeam* SpawnBeam(FTransform SpawnTransform, float BeamLength, bool bIsPrimary, bool bIsSecondary, float SpeedMultiplier);

    void OnSpawnFirewall(int32 Direction, int32 Position, float Width, float SpeedMultiplier, bool bDoubleColor);
    void OnSpawnBurst(int32 Direction, int32 Position, int32 BurstDistance, float SpeedMultiplier, bool bDoubleColor);
    void OnSpawnBeam(int32 Direction, int32 Position, float SpeedMultiplier, bool bDoubleColor);

    UFUNCTION()
    void UpdatePlayerTarget();

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

private:
    TArray<FTimerHandle> SpawnTimerHandles;

};
