// Fill out your copyright notice in the Description page of Project Settings.

#include "TopMoverGameModeBase.h"
#include "Kismet/GameplayStatics.h"
#include "Virus.h"
#include "TDPlayerState.h"
#include "TMGameState.h"
#include "Kismet/GameplayStatics.h"
#include "Engine/World.h"
#include "EngineUtils.h"
#include "FirewallBase.h"

ATopMoverGameModeBase::ATopMoverGameModeBase() {
    StartingBombCount = 3;
    StartingLifeCount = 3;
    PlayerStateClass = ATDPlayerState::StaticClass();
    GameStateClass = ATMGameState::StaticClass();

    GameTime = 0.f;
    bGameTimerActive = false;
}

void ATopMoverGameModeBase::BeginPlay() {
    Super::BeginPlay();
    GetPlayerState(0)->Lives = StartingLifeCount;
}

void ATopMoverGameModeBase::Tick(float DeltaSeconds) {
    Super::Tick(DeltaSeconds);
    if (bGameTimerActive) {
        GameTime += DeltaSeconds;
    }
}

void ATopMoverGameModeBase::RestartPlayer(AController* NewPlayer) {
    Super::RestartPlayer(NewPlayer);
    AVirus* PlayerVirus = Cast<AVirus>(NewPlayer->GetPawn());
    if (!PlayerVirus) {
        UE_LOG(LogTemp, Error, TEXT("Player was restarted but had no pawn!"));
        return;
    }
    if (PlayerVirus->IsActorBeingDestroyed()) {
        OnPlayerKilled();
    }
    else {
        PlayerVirus->OnKilled.AddDynamic(this, &ATopMoverGameModeBase::OnPlayerKilled);
    }
}

void ATopMoverGameModeBase::OnPlayerKilled() {
    PauseProgress();
    LosePlayerLife();
    if (GetPlayerState(0)->Lives <= 0) {
        GameOver();
    }
    else {
        RespawnPlayer();
    }
}

void ATopMoverGameModeBase::ClearFirewalls(bool bPhaseFirewallsOut) {
    UWorld* World = GetWorld();
    check(World);
    for (TActorIterator<AFirewallBase> FirewallIt(World); FirewallIt; ++FirewallIt) {
        if (bPhaseFirewallsOut) (*FirewallIt)->PhaseOut();
        else (*FirewallIt)->Destroy();
    }
}

void ATopMoverGameModeBase::LosePlayerLife() {
    GetPlayerState(0)->Lives -= 1;
    OnLifeLost();
}

ATDPlayerState* ATopMoverGameModeBase::GetPlayerState(int32 PlayerIndex) {
    APlayerController* PlayerController = UGameplayStatics::GetPlayerController(this, PlayerIndex);
    if (!PlayerController) return nullptr;
    return PlayerController->GetPlayerState<ATDPlayerState>();
}

void ATopMoverGameModeBase::PauseGameTimer() {
    bGameTimerActive = false;
}

void ATopMoverGameModeBase::ResumeGameTimer() {
    bGameTimerActive = true;
}

void ATopMoverGameModeBase::RestartGameTimer() {
    bGameTimerActive = true;
    GameTime = 0.f;
}
