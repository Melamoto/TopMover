// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "FirewallBase.generated.h"

UCLASS(abstract)
class TOPMOVER_API AFirewallBase : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AFirewallBase();

    // Called when the game starts or when spawned
    virtual void BeginPlay() override;

    // Called every frame
    virtual void Tick(float DeltaTime) override;

    UFUNCTION(BlueprintCallable, BlueprintImplementableEvent, Category = "Firewall")
    void PhaseOut();

    UPROPERTY(BlueprintReadWrite, EditAnywhere, Meta = (Category = "Firewall", ExposeOnSpawn))
    float TravelSpeed;
    UPROPERTY(BlueprintReadWrite, EditAnywhere, Meta = (Category = "Firewall", ExposeOnSpawn))
    float TravelLength;
    UPROPERTY(BlueprintReadWrite, EditAnywhere, Meta = (Category = "Firewall", ExposeOnSpawn, DisplayName = "Is Primary?"))
    uint32 bIsPrimary : 1;
    UPROPERTY(BlueprintReadWrite, EditAnywhere, Meta = (Category = "Firewall", ExposeOnSpawn, DisplayName = "Is Secondary?"))
    uint32 bIsSecondary : 1;
    UPROPERTY(BlueprintReadWrite, EditAnywhere, Meta = (Category = "Firewall", ExposeOnSpawn, DisplayName = "Is Active?"))
    uint32 bIsActive: 1;

    UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Movement")
    FRotator RotationOffset;
    UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Movement")
    float RotationRate;

    UFUNCTION(BlueprintPure, BlueprintNativeEvent, Category = "Movement")
    USceneComponent* GetBody();

    void RotateBodyTowardsVelocity(float DeltaSeconds);
};
